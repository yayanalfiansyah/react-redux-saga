import React from 'react'

const ShowData = (props) => {

    const { data } = props;

    console.log(data);

    return (
        <span>
            {data && data.map(item => {
                return (
                    <div className="card" key={item.id} style={{ marginTop: '10px' }}>
                        <div className="card-body">
                            <h5 className="card-title">
                                {item.title}
                            </h5>
                            <p className="card-text">
                                {item.body}
                            </p>
                        </div>
                    </div>
                )
            })}
        </span>
    )
}


export default ShowData