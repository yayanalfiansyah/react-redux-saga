import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Test from './test/Test'
import Index from './default/DefaultHome'
import Sidebar from './homeComponent/Sidebar'

class Home extends Component {

    render() {

        return (
            <div className="bmd-layout-container bmd-drawer-f-l">
                <header className="bmd-layout-header">
                    <div className="navbar navbar-light bg-faded">
                        <button className="navbar-toggler" type="button" data-toggle="drawer" data-target="#dw-s1">
                            <span className="sr-only">Toggle drawer</span>
                            <i className="material-icons">menu</i>
                        </button>
                        <ul className="nav navbar-nav">
                            <li className="nav-item">Title</li>
                            <li className="nav-item">Something</li>
                        </ul>
                    </div>
                </header>
                <div id="dw-s1" style={{ position: 'fixed' }} className="bmd-layout-drawer bg-faded">
                    <Sidebar />
                </div>
                <main className="bmd-layout-content">
                    <div className="container">
                        <Router>
                            <Switch>
                                <Route exact path="/home/" component={Index} />
                                <Route path="/home/test" component={Test} />
                            </Switch>
                        </Router>
                    </div>
                </main>
            </div>
        )
    }
}

export default Home