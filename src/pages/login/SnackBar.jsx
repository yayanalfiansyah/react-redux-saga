import React, { Component } from 'react'

class SnackBar extends Component {

    render() {
        let text = "text"
        return (
            <button
                type="button"
                className="btn btn-secondary"
                data-toggle="snackbar"
                data-content={text}
                data-html-allowed="true"
                data-timeout="1000"
                onClick={this.handleClick}
            >
                Snackbar
        </button>
        )
    }

    handleClick = () => {
        console.log("test")
    }
}

export default SnackBar