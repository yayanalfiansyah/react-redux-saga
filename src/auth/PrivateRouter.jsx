import React from 'react';
import { Redirect, Route } from 'react-router-dom';


// this is function where you should define any role or auth
function isAuth() {
    return true
}

const PrivateRoute = ({ component: Component, ...rest }) => {

    return (
        <Route
            {...rest}
            render={props =>
                isAuth() ? (
                    <Component {...props} />
                ) : (
                        <Redirect
                            to={
                                {
                                    pathname: "/login",
                                    state: { from: props.location }
                                }
                            }
                        />
                    )
            }
        />
    )
}

export default PrivateRoute