import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import Login from './pages/login/Login'
import Home from './pages/home/Home'
import PrivateRoute from './auth/PrivateRouter'

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      value: ""
    }

  }

  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/login" component={Login} />
          <PrivateRoute path="/home" component={Home} />
          <Redirect from="/" to="/login" />
        </Switch>
      </Router>
    );
  }
}

export default App;
