import { takeLatest, take, takeEvery, call, put, fork } from 'redux-saga/effects'
import axios from 'axios'

export default function* testSaga() {
    yield takeLatest("API_CALL_REQUEST", workerSaga);
}

export function* testSaga1() {
    yield takeLatest("API_TEST", testOnly)
}

function testOnly() {
    console.log("test Only")
}

function fetchData() {
    let url = "https://jsonplaceholder.typicode.com/posts"
    console.log(url);
    return axios({
        method: "GET",
        url: url
    });
}

function* workerSaga(action) {
    try {
        const response = yield call(fetchData);
        const data = response.data;
        console.log("worker saga");
        console.log(action);
        yield put({
            type: "API_CALL_SUCCESS", data
        })
    } catch (error) {
        console.log(error);
        yield put({
            type: "API_CALL_FAILED", error
        })
    }
}